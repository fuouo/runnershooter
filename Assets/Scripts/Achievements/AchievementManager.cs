﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/*
 * Achievement Manager that is incharge of triggering achievements, and restarting achievement stats
 * Created by Dyan Nieva
 */
public class AchievementManager : MonoBehaviour {

	//Parameter Key/s
	//Tag for Achievement Tags. Used for passing Achievement Tags as a parameter
	public const string ACHIEVEMENT_TAG_KEY = "ACHIEVEMENT_TAG_KEY";

	ObjectPool objectPooler;
	private static AchievementManager instance;
	public static AchievementManager Instance {
		get {
			return instance;
		}
	}

	void Awake(){
		instance = this;
	}
		
	private Dictionary<string, Achievement> achievementDictionary; //Dictionary of all Achiemvements

	void Start () {
		objectPooler = ObjectPool.Instance;
		this.achievementDictionary = new Dictionary<string, Achievement> ();

		//Called when an achievement is unlocked, like "Detecting Input", etc
		Broadcaster.Instance.AddObserver (Events.ON_TRIGGER_ACHIEVEMENT, this.OnTriggerAchievement); 

		//Stores all achievements in the achievementDictionary
		foreach (Achievement a in Achievement.LoadAll ()) {
			achievementDictionary.Add (a.tag, a);
		}
	}


	void OnApplicationQuit()
	{
		//To save the status of achievements, pls comment this code out :)
		if (GameManager.Instance.CanRestartOnQuit ()) {
			RestartAchievements ();
		}
	}


	/// <summary>
	/// Unlocks all achievements when the game is closed. 
	/// </summary>
	void RestartAchievements(){
		Debug.Log ("Restart All Achievements");
		foreach (Achievement a in Achievement.LoadAll ()) {
			a.isUnlocked = false;
		}
	}

	/// <summary>
	/// An Observer. Is called to trigger achievements in the UI.
	/// </summary>
	/// <remarks>
	/// If achievement exists from dictionary
	/// this method will request for an Achievement to show from the objectpool (passing its Achievement details), and unlocks the achievement
	/// </remarks>
	/// <param name="param">Parameter. Should store an Achievement tag in string</param>
	void OnTriggerAchievement(Parameter param){
		string tag = param.GetStringExtra (ACHIEVEMENT_TAG_KEY, "");

		//Checks if there is such achievement with the existing tag
		if (tag.Equals ("") || !achievementDictionary.ContainsKey (tag)) {
			return;
		}

		//Checks if the existing achievement is unlocked
		if (achievementDictionary [tag].isUnlocked)
			return;

		Broadcaster.Instance.PostEvent (Events.ON_PLAY_ACHIEVE); //Play Achievement Audio

		achievementDictionary [tag].isUnlocked = true; //Unlocks achievement

		//Requests from the object pool for a UI object. In requesting, objectpooler will call OnObjectSpawn of AchievementUI
		objectPooler.SpawnFromPool ("ACHIEVEMENT", achievementDictionary [tag], transform.position, Quaternion.identity);
	}
}
