﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/* 
 * Parent class for any AudioHandlers (SFX and BGM). Has reference to audiosources
 */ 
public class AudioHandler : MonoBehaviour {

	[SerializeField] AudioSource audioSource;

	protected virtual void PlaySound(AudioClip clip, bool onLoop){
		audioSource.clip = clip;
		audioSource.Play ();
		audioSource.loop = onLoop;
	}

}
