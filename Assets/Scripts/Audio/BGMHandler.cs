﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BGMHandler : AudioHandler {

	[SerializeField] AudioClip IntroClip; //BGM for On start of game
	[SerializeField] AudioClip RunningClip; //BGM when player starts running
	[SerializeField] AudioClip GameOverClip; //BGM when player dies

	void Start () {
		Broadcaster.Instance.AddObserver(Events.ON_INTRO, this.OnIntro); 
		Broadcaster.Instance.AddObserver(Events.ON_START_RUN, this.OnStartRun);
		Broadcaster.Instance.AddObserver (Events.ON_DEAD, this.OnDead);
	}

	void OnIntro(){
		PlaySound (IntroClip, true);
	}

	void OnStartRun(){
		PlaySound (RunningClip, true);
	}

	void OnDead(){
		PlaySound (GameOverClip, false);
	}

}
