﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * This class is for storing references for the enemies that is 
 * part of the combination for the platform
 * Created by Dyan Nieva
 */ 

public class EnemyCombination : MonoBehaviour {

	[SerializeField] Enemy[] enemies; //Enemies part of the enemy combination

	/// <summary>
	/// Reset all enemies that is part of this enemy combination on spawning the platform 
	/// </summary>
	public void ResetAllEnemies(){
		foreach (Enemy enemy in enemies) {
			enemy.ResetEntity ();
			enemy.gameObject.SetActive (true);
		}
	}


}
