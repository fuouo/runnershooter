﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

/*
 * ObjectPool, stores all objectpoolers, and in charge of requesting for a spawn of an objectpooler
 * Created by Dyan Nieva
 */
public class ObjectPool : MonoBehaviour {
	
	/// <summary>
	/// ObjectPools that store their unique tags, object containers, prefab for objects to spawn, and size of the pool
	/// </summary>
	[System.Serializable]
	public class Pool{
		public string tag;
		public GameObject parent;
		public GameObject prefab;
		public int size;
	}

	private static ObjectPool instance;
	public static ObjectPool Instance {
		get {
			return instance;
		}
	}

	void Awake(){
		instance = this;
	}

	//for all objet pools
	public List<Pool> pools;

	//for queues of all pools
	public Dictionary<string, Queue<GameObject>> poolDictionary;


	void Start(){
		poolDictionary = new Dictionary<string, Queue<GameObject>>();
	
		foreach (Pool pool in pools) {
			Queue<GameObject> objectPool = new Queue<GameObject> ();

			//instantiates all objects for all pools for all size, and adds it to queue
			for (int i = 0; i < pool.size; i++) {
				GameObject obj = Instantiate (pool.prefab);
				obj.transform.SetParent (pool.parent.transform);
				obj.SetActive (false);
				objectPool.Enqueue (obj);
				obj.transform.localScale = Vector3.one;
			}

			poolDictionary.Add (pool.tag, objectPool);
		}
	}

	/// <summary>
	/// Spawn an object from the pool[tag]
	/// </summary>
	/// <returns>The spawned object</returns>
	/// 
	/// <param name="tag">Unique Tag of the Pool.</param>
	/// <param name="position">Position to instantiate the object</param>
	/// <param name="rotation">Rotation to instantiate the object</param>
	public GameObject SpawnFromPool(string tag, Vector3 position, Quaternion rotation){
		if (!poolDictionary.ContainsKey (tag)) {
			Debug.LogWarning ("Pool with tag " + tag + " does not exist :)");
			return null;
		}
	
		//removes the oldest object from the queue of pool
		GameObject objectToSpawn = poolDictionary [tag].Dequeue ();

		//activates object
		objectToSpawn.SetActive (true);
		objectToSpawn.transform.position = position;
		objectToSpawn.transform.rotation = rotation;
		objectToSpawn.transform.localScale = Vector3.one;

		//spawns object 
		IPoolableObject pooledObj = objectToSpawn.GetComponent<IPoolableObject> ();
		if (pooledObj != null) {
			pooledObj.OnObjectSpawn ();
		}

		//adds the object back to queue as the newest object of the poole
		poolDictionary[tag].Enqueue(objectToSpawn);

		return objectToSpawn;
	}

	/// <summary>
	/// Resets the pool queue of the given tag
	/// </summary>
	/// <param name="tag">Tag of the pool</param>
	public void ResetPool(string tag){
		var tempQueue = poolDictionary [tag]; 
		tempQueue = new Queue<GameObject> ();

		foreach (var item in tempQueue.Reverse()) {
			poolDictionary [tag].Enqueue(item);
		}
	}

	/// <summary>
	/// Spawn an object, given a reference, from the pool[tag]
	/// </summary>
	/// <returns>The spawned object</returns>
	/// <param name="tag">Unique Tag of the Pool.</param>
	/// <param name="reference">Reference object for the object spawned</param>
	/// <param name="position">Position to instantiate the object</param>
	/// <param name="rotation">Rotation to instantiate the object</param>
	public GameObject SpawnFromPool(string tag, object reference, Vector3 position, Quaternion rotation){
		if (!poolDictionary.ContainsKey (tag)) {
			Debug.LogWarning ("Pool with tag " + tag + " does not exist :)");
			return null;
		}

		//removes the oldest object from the queue of pool
		GameObject objectToSpawn = poolDictionary [tag].Dequeue ();

		//activates object
		objectToSpawn.SetActive (true);
		objectToSpawn.transform.position = position;
		objectToSpawn.transform.rotation = rotation;
		objectToSpawn.transform.localScale = Vector3.one;

		//spawns object with the reference
		IPoolableObject pooledObj = objectToSpawn.GetComponent<IPoolableObject> ();
		if (pooledObj != null) {
			pooledObj.OnObjectSpawn (reference);
		}

		//adds the object back to queue as the newest object of the pool
		poolDictionary[tag].Enqueue(objectToSpawn);

		return objectToSpawn;
	}


	/// <summary>
	/// Removes the object from the pool
	/// </summary
	/// <param name="tag">Unique tag of the pool.</param>
	/// <param name="obj">Object to remove from the pool</param>
	public void RemoveFromPool(string tag, GameObject obj){
		if (!poolDictionary.ContainsKey (tag)) {
			Debug.LogWarning ("Pool with tag " + tag + " does not exist :)");
			return;
		}
		obj.SetActive (false);
		//TODO: Remove from queue
	}
}
