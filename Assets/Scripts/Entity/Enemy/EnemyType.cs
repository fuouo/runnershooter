﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * EnemyType, for all enemies of any type
 * Groups all enemies of any type (runner / shooter)
 * Created by Dyan Nieva
 */
public class EnemyType : EntityType {

	public float maxDistanceFromPlayer;
	public float minDistanceFromPlayer;

	/// <summary>
	/// Loads all EnemyType data in Resources/Enemies folder
	/// </summary>
	/// <returns>All Enemies</returns>
	public static EnemyType[] LoadAll() {
		return Resources.LoadAll<EnemyType>("Enemies");
	}
}
