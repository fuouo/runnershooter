﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * Sound Effects Handler. Child of AudioHandler
 * Creted by Dyan Nieva
 */ 
public class SFXHandler : AudioHandler {

	[SerializeField] AudioClip ButtonClickClip;
	[SerializeField] AudioClip WhistleClip;
	[SerializeField] AudioClip AchievementClip;

	void Start () {
		Broadcaster.Instance.AddObserver(Events.ON_BUTTON_CLICK, this.OnClick); //Plays click when UI buttons are clicked
		Broadcaster.Instance.AddObserver(Events.ON_WHISTLE, this.OnWhistle); //Plays whistle when the player starts running
		Broadcaster.Instance.AddObserver(Events.ON_PLAY_ACHIEVE, this.OnAchieve); //Plays audio when an achievement is triggered

	}

	void OnClick(){
		PlaySound (ButtonClickClip, false);
	}

	void OnWhistle(){
		PlaySound (WhistleClip, false);
	}
		
	void OnAchieve(){
		PlaySound (AchievementClip, false);
	}
				
}
