﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/*
 * UIManager for GameOverScreen = Incharge of replaying, and updating score /enemies killeds
 * Created by Dyan Nieva
 */ 
public class GameOverScreen : UIScreen {

	[SerializeField] Text scoreText;

	void Start(){
	}

	/// <summary>
	/// Updates the score / Enemies killed by Paramter that stores the number of enemies kiled
	/// </summary>
	/// <param name="param">Parameter that stores the number of Enemies killed.</param>
	public void UpdateScore(int score){
		scoreText.text = score + "";
	}

	/// <summary>
	/// Calls classes that needs to be restarted on replay of the game
	/// </summary>
	public void OnReplay(){
		Broadcaster.Instance.PostEvent (Events.ON_REPLAY);
	}

	/// <summary>
	/// Calls classes that needs to be restarted on character select
	/// </summary>
	public void OnCharacterSelect(){
		Broadcaster.Instance.PostEvent (Events.ON_CHARACTER_SELECT);
	}
}
