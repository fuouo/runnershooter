﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


/*
 * UI Manager of InGameScreen. In charge of keeping track of score / enemies killed
 * Created By Dyan Nieva
 */ 
public class InGameScreen : UIScreen {
	[Header("Score")]
	[SerializeField] Text score; //displays the number of enemies killed

	// Use this for initialization
	void Start () {
		Broadcaster.Instance.AddObserver (Events.ON_ADD_SCORE, this.AddScore); //Called by GameManager whenever enemy is killed
	}

	/// <summary>
	/// Increments the score.
	/// </summary>
	void AddScore(){
		int newScore = int.Parse (score.text) + 1;
		score.text = newScore + "";
	}


	/// <summary>
	/// Resets the score in the UI
	/// </summary>
	/// <param name="score">Score.</param>
	public void ResetScore(){
		score.text = "0";
	}
}
