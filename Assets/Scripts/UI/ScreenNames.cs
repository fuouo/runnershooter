﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/*
 * Storage of all screen names. 
 * ScreenNames must be unique
 * Created By Dyan Nieva
 */

public class ScreenNames {
	public const string CharacterSelectionScreen = "CharacterSelectScreen";
	public const string InGameScreen = "InGameScreen";
	public const string GameOverScreen = "GameOverScreen";

}
