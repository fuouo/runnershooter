﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/*
 * ShooterType for all shooter enemies
 * ShooterType has reference to shooter prefab, shootSpeed and bulletSpeed
 * Created by Dyan Nieva
 */
[CreateAssetMenu(fileName = "EnemyType", menuName = "Game/Enemy/Shooter", order = 2)]
public class ShooterType : EnemyType {
	public float shootDelay;
	public float bulletShootDuration;

	//prefab of the shooter gameobject
	public GameObject shooter;

	/// <summary>
	/// Loads all ShooterType data in Resources/Enemies folder
	/// </summary>
	/// <returns>All ShooterType</returns>
	public static ShooterType[] LoadAll() {
		return Resources.LoadAll<ShooterType>("Enemies");
	}

}
