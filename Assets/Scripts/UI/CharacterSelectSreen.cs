﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/*
 * UIManager for the CharacterSelectionScreen
 * Created By Dyan Nieva
 */ 
public class CharacterSelectSreen : UIScreen {

	[SerializeField] Text playerName; //reference for the character name 
	[SerializeField] Text playerDescription; //reference for the character description

	[SerializeField] GameObject characterContainer; //Contains the models of different character types

	PlayerType[] playerTypes;

	int currentCounter = 0; //index of the current character in CharacterSelection

	bool isInitialized = false;

	// Use this for initialization
	void Start () {
		Broadcaster.Instance.AddObserver (Events.ON_CHARACTER_SELECT, this.Initialize); //if game restarts, Initialize is called
		Initialize ();
	}

	/// <summary>
	/// Initializing the playerType models.
	/// </summary>
	void Initialize ()
	{
		//Hide the Character Container
		characterContainer.SetActive (true);

		playerTypes = PlayerType.LoadAll ();

		if (!isInitialized) {
			//instantiates all playertype models in the characterContainer
			foreach (PlayerType type in playerTypes) {
				var obj = Instantiate (type.model, 
					          characterContainer.transform.position,
					          characterContainer.transform.rotation,
					          characterContainer.transform);
				obj.transform.SetSiblingIndex (1); //sets index to 1, so that the newly instantiated object will always be last, 
				//and will align with the playerTypes order of characters
				obj.SetActive (false);
			}
			isInitialized = true;
		} else {
			for (int i = 0; i < characterContainer.transform.childCount; i++) {
				var obj = characterContainer.transform.GetChild (i);
				obj.gameObject.SetActive (false);
			}

		}

		characterContainer.transform.GetChild (currentCounter).gameObject.SetActive (true);
		SetDescription ();
	}

	/// <summary>
	/// Called when the Play button is clicked.
	/// <remarks>
	/// Passes the selected Player Type to the Player, and calls GameManager's OnPlay, to start playing
	/// </remarks>
	/// </summary>
	public void OnPlay(){

		Parameter param = new Parameter ();
		param.PutObjectExtra (Player.TYPE_KEY, (PlayerType) playerTypes[currentCounter]);
		Broadcaster.Instance.PostEvent (Events.ON_SET_PLAYER_TYPE, param);
		//Play UI Button Click
		Broadcaster.Instance.PostEvent (Events.ON_BUTTON_CLICK);

		//Hide the Character Container
		characterContainer.SetActive (false);

		//Calls GameManager to start playing
		Broadcaster.Instance.PostEvent (Events.ON_PLAY);


	}


	/// <summary>
	/// Moves to the next character in Character Container. Changes the title and description.
	/// </summary>
	public void OnNextCharacter(){
		characterContainer.transform.GetChild (currentCounter).gameObject.SetActive (false);
		if (currentCounter + 1 == playerTypes.Length)
			currentCounter = 0;
		else
			currentCounter++;
		characterContainer.transform.GetChild (currentCounter).gameObject.SetActive (true);

		SetDescription ();

		//Play UI Button Click
		Broadcaster.Instance.PostEvent (Events.ON_BUTTON_CLICK);
	}

	/// <summary>
	/// Moves to the previous character in Character Container. Changes the title and description.
	/// </summary>
	public void OnPrevCharacter(){
		characterContainer.transform.GetChild (currentCounter).gameObject.SetActive (false);
		if (currentCounter - 1 == -1)
			currentCounter = playerTypes.Length - 1;
		else
			currentCounter--;
		characterContainer.transform.GetChild (currentCounter).gameObject.SetActive (true);
		SetDescription ();

		//Play UI Button Click
		Broadcaster.Instance.PostEvent (Events.ON_BUTTON_CLICK);



	}

	/// <summary>
	/// Sets the description and title of the character based from the current player type
	/// </summary>
	void SetDescription(){
		playerName.text = playerTypes [currentCounter].typeName;
		playerDescription.text = playerTypes [currentCounter].description;
	}


}
