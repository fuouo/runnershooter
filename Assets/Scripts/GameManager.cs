﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


/* 
 * Manages the global function of the game (OnPlay, OnDead, OnReplay, scoring, etc)
 * Created By Dyan Nieva
 */ 
public class GameManager : MonoBehaviour {
	
	//Key for Score / Enemies Killed
	public const string SCORE_KEY = "SCORE_KEY";

	//Resolution
	[SerializeField] int screenWidth; //450
	[SerializeField] int screenHeight; //800

	[SerializeField] private bool isPlaying = false;
	[SerializeField] private bool canRestartOnQuit = false; //restarts all persistent stats on Quit (i.e. Achievement unlock status, etc)

	[SerializeField] private int enemiesKilled = 0;

	private static GameManager instance;
	public static GameManager Instance {
		get {
			return instance;
		}
	}

	void Awake(){
		instance = this;
		Screen.SetResolution (screenWidth, screenHeight, false);
	}

	// Use this for initialization
	void Start () {
		Broadcaster.Instance.PostEvent (Events.ON_INTRO); //Play Intro Music

		Broadcaster.Instance.AddObserver (Events.ON_PLAY, this.OnPlay); //Called when Character is Selected 
		Broadcaster.Instance.AddObserver (Events.ON_ENEMY_KILLED, this.AddToEnemiesKilled); //Called when an enemy is killed
		Broadcaster.Instance.AddObserver (Events.ON_DEAD, this.OnDead); //CAlled when Player dies 
		Broadcaster.Instance.AddObserver (Events.ON_REPLAY, this.OnReplay); //Called when the game is restarted from the GameOverScreen
		Broadcaster.Instance.AddObserver (Events.ON_CHARACTER_SELECT, this.OnCharacterSelect); //Called when the game goes back to CharacterSelect from GameOverScreen



		
	}
	
	// Update is called once per frame
	void Update () {
	}

	/// <summary>
	/// Shows InGameScreen, and sets isPlaying = true
	/// </summary>
	void OnPlay(){
		StartCoroutine (DelayPlay ());	//delays the play to make way for animations and the like
		
	}

	/// <summary>
	/// Delays Playing to make way for animation. Plays audio clip to start running;
	/// </summary>
	IEnumerator DelayPlay(){
		var screen = ScreenHandler.Instance.Show (ScreenNames.InGameScreen);
		((InGameScreen)screen).ResetScore ();
		yield return new WaitForSeconds (0.5f); 
		isPlaying = true;

		//Plays a whistle clip to start running
		Broadcaster.Instance.PostEvent (Events.ON_WHISTLE);
		Broadcaster.Instance.PostEvent (Events.ON_START_RUN);

	}


	/// <summary>
	/// Shows GameOverScreen, and sets isPlaying = false
	/// </summary>
	void OnDead(){
		if (!isPlaying)
			return;
		
		StartCoroutine(DelayDead());

	}

	IEnumerator DelayDead(){
		yield return new WaitForSeconds (0.5f);

		var screen = ScreenHandler.Instance.Show (ScreenNames.GameOverScreen);
		((GameOverScreen)screen).UpdateScore (enemiesKilled);

		isPlaying = false;

	}

	/// <summary>
	/// Sets enemiesKilled back to 0 and plays again.
	/// </summary>
	void OnReplay(){
		enemiesKilled = 0;
		OnPlay ();
	}

	void OnCharacterSelect(){
		var screen = ScreenHandler.Instance.Show (ScreenNames.CharacterSelectionScreen);
		enemiesKilled = 0;

		Broadcaster.Instance.PostEvent (Events.ON_INTRO); //Play Intro Music
	}

	/// <summary>
	/// When an enemy dies, enemiesKilled increments to 1. 
	/// <remarks>
	/// Triggers 'ENEMY_KILLED' achievement when first time killed enemy. Notifies InGameScreen to add to score.
	/// </remarks> 
	/// </summary>
	void AddToEnemiesKilled(){
		enemiesKilled++; 

		if (enemiesKilled == 1) {
			Parameter achievementParam = new Parameter ();
			achievementParam.PutExtra (AchievementManager.ACHIEVEMENT_TAG_KEY, "ENEMY_KILLED");
			Broadcaster.Instance.PostEvent(Events.ON_TRIGGER_ACHIEVEMENT, achievementParam);
		}

		Broadcaster.Instance.PostEvent (Events.ON_ADD_SCORE);

	}


	public bool IsPlaying()
	{
		return this.isPlaying;
	}

	public bool CanRestartOnQuit(){
		return canRestartOnQuit;
	}


}
