﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * PlayerShooter. Shooter class for the player. 
 * Shoots bullet when LMB is clicked. 
 * Created by Dyan Nieva
 */ 
public class PlayerShooter : ShooterController {

	bool firstTimeShoot = true;

	// Use this for initialization
	void Start () {
		TAG = "PLAYER_SHOOTER"; //for objectpool
		objectPooler = ObjectPool.Instance;
		Broadcaster.Instance.AddObserver (Events.ON_SHOOT, this.OnShoot); //Called by InputHandler when LMB is clicked
		Broadcaster.Instance.AddObserver (Events.ON_REST, this.OnRest); //Called by InputHandler when LMB is up
	}

	protected override void Update(){
		base.Update ();

		if (!isShooting) {
			if (!timeIsUp) {
				InputHandler.Instance.SetClickMouse (false);
			} else {
				InputHandler.Instance.SetClickMouse (true);
			}
		}
	}
		
	/// <summary>
	/// When LMB is clicked, player shoots
	/// </summary>
	protected override void OnShoot(){
		
		if (firstTimeShoot) { //Triggers the Achievement "Detect Input";
			Parameter param = new Parameter ();
			param.PutExtra (AchievementManager.ACHIEVEMENT_TAG_KEY, "PLAYER_SHOOT");
			Broadcaster.Instance.PostEvent (Events.ON_TRIGGER_ACHIEVEMENT, param);
			firstTimeShoot = false;
		}
			
		base.OnShoot ();
	}

	/// <summary>
	/// When LMB is up, shooting stops
	/// </summary>
	void OnRest(){
		isShooting = false;
		canRestartTimer = false;
	}

	/// <summary>
	/// Requests from the objectpool to spawn a bullet, and set its source to the respective shootercontroller
	/// </summary>
	protected override void SpawnBullets(){
		GameObject bullet = objectPooler.SpawnFromPool ("PLAYER_SHOOTER", transform.position, Quaternion.identity);
		bullet.GetComponent<Bullet> ().SetSource (this);
	}



}
