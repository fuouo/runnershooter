﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/* 
 * UIManager for HPBar in charge of updating hp bars, and following entities
 * Created by Dyan Nieva
 */
public class HPBar : MonoBehaviour {

	[SerializeField] Vector3 offset; //offset of the position of the HP

	[SerializeField] Image fill; //HP Fill

	/// <summary>
	/// Follows the entity and repositions HP bar based from offset
	/// </summary>
	/// <param name="entity">Entity to follow</param>
	public void FollowEntity(Entity entity){
		var newPosition = entity.transform.position + offset;
		transform.position = newPosition;
		//		hpFill.transform.parent.parent.position = new Vector3 (transform.position.x, 1.5f, transform.position.z);
	}

	/// <summary>
	/// Updates the HP fill.
	/// </summary>
	/// <param name="newHP">Entity HP (%)</param>
	public void UpdateFill(float newHP){
		fill.fillAmount = newHP / 100;
	}
}
