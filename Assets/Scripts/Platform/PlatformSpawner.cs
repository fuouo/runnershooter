﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/*
 * PlatformSpawner. In charge of spawning platforms, and knowing the previous and currentPlatform.
 * This is implemented on a Platform Detector (Detects whether platform needs to be spawned or not).
 * Created By Dyan Nieva
 */
public class PlatformSpawner : MonoBehaviour {


	public static string TAG = "PLATFORM_SPAWNER";

	[SerializeField] private bool shouldSpawn = true;
	[SerializeField] private bool isOnFloor = false; //to detect whether the platform detector is on the floor. change through OnCollision methd

	private static PlatformSpawner instance;
	public static PlatformSpawner Instance {
		get {
			return instance;
		}
	}

	void Awake(){
		instance = this;
	}

	GameObject previousPlatform;
	[SerializeField] GameObject firstPlatform; //this is used once the level start, to also restart the queue of the pooling.
	[SerializeField] GameObject currentPlatform;
	ObjectPool objectPooler;

	void Start(){
		objectPooler = ObjectPool.Instance;
		Broadcaster.Instance.AddObserver (Events.ON_PLATFORM_SPAWN, this.SpawnNextPlatform);
		Broadcaster.Instance.AddObserver (Events.ON_REPLAY, this.Initialize); //if game restarts, Initialize is called
		Broadcaster.Instance.AddObserver (Events.ON_CHARACTER_SELECT, this.Initialize); //if game goes to character select, Initialize is called


	}

	void Update(){

		//if isOnFloor is true, shouldspawn = true
		shouldSpawn = !isOnFloor;

		if (shouldSpawn) {
			SpawnNextPlatform ();
			shouldSpawn = false;
		}
	}

	/// <summary>
	/// Called when the level restarts. Restarts the currentPlatform to firstPlatform, and other attributes.
	/// </summary>
	void Initialize(){
		objectPooler.ResetPool ("PLATFORM_SPAWNER");
		currentPlatform = firstPlatform;
		previousPlatform = currentPlatform;
		shouldSpawn = true;
		isOnFloor = false;

	}

	/// <summary>
	/// Requests from objectpooler for a platform, and sets currentPlatform to newly pulled object from pool
	/// </summary>
	void SpawnNextPlatform(){
		previousPlatform = currentPlatform;
		currentPlatform = objectPooler.SpawnFromPool ("PLATFORM_SPAWNER", transform.position, Quaternion.identity);
	}

	public GameObject getPreviousPlatform(){
		return previousPlatform;
	}

	public GameObject getCurrentPlatform(){
		return currentPlatform;
	}

	/// <summary>
	/// Gets the size (x,y,z) of the platform.
	/// </summary>
	/// <returns>The platform size (x, y, z)</returns>
	public Vector3 getPlatformSize(){
		return currentPlatform.GetComponent<Platform>().getCollider().bounds.size;
	}
		

	/// <summary>
	/// Platform Spawner is a trigger. When this detects a floor, should not spawn platforms.
	/// Once Player is far enought that detector does not detect any platforms, should spawn.
	/// </summary>
	/// <param name="collision">Object detector is colliding with</param>
	void OnTriggerEnter(Collider collision){
		if (collision.tag == "Floor") {
			isOnFloor = true;
		}
	}

	/// <summary>
	/// If the platform detector is still on Platform, should not spawn.
	/// </summary>
	/// <param name="collision">Object detector is colliding with</param>
	void OnTriggerStay(Collider collision){
		if (collision.tag == "Floor") {
			isOnFloor = true;
		}
	}

	/// <summary>
	/// Once the detector is outside of platform, should spawn
	/// </summary>
	/// <param name="collision">Object detector is colliding with</param>
	void OnTriggerExit(Collider collision){
		if (collision.tag == "Floor") {
			isOnFloor = false;
			
		}
	}

}
