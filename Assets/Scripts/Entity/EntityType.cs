﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * Superclass for all EntityType (PlayerType / EnemyType (ShooterType/RunnerType) )
 * Created by Dyan Nieva
 */ 
public class EntityType : ScriptableObject {

	//data
	public string id; //unique id of the type (no space pls :))
	public string typeName; //name of the type
	public string description; //description of the type
	public float fwMovementDuration; //duration to move forward in one platform
	public float damage; //damage of the type

	//art / assets
	public GameObject model; //model of the type
}
