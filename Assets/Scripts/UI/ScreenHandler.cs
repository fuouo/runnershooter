﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * Handles the transition from screen to screen
 * Created By Dyan Nieva
 */
public class ScreenHandler : MonoBehaviour {

	private static ScreenHandler instance;
	public static ScreenHandler Instance {
		get {
			return instance;
		}
	}

	void Awake() {
		instance = this;
	}

	[SerializeField] public UIScreen activeScreen; 
	[SerializeField] private UIScreen[] screenList; //list of all screen game objects 
	private Dictionary<string, UIScreen> screenDictionary; //dictionary of all screens

	void Start(){
		//shows the active screen first
		activeScreen.Show ();

		//Assigns each screen from screenlist to screenDictionary, with the screenName as tag/s
		screenDictionary = new Dictionary<string, UIScreen>();
		foreach (UIScreen screen in screenList) {
			screenDictionary.Add (screen.name, screen);
		}
	}


	/// <summary>
	/// Hides the current activeScreen, shows the screen with screenName, and sets activeScreen.
	/// </summary>
	/// <returns> The new active screen </returns>
	/// <param name="screenName">Screen name.</param>
	public UIScreen Show(string screenName) {
		
		//hides current active screen
		activeScreen.Hide ();

		//If screen with screenName exists, set this to the active screen, and show the screen
		if (screenDictionary.ContainsKey (screenName)) {
			activeScreen = screenDictionary [screenName];
			activeScreen.Show ();
		}

		return activeScreen;

		
	}
}
