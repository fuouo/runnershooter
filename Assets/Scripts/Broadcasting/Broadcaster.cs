﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * Event Broadcaster that uses event names to call events
 * Created By Dyan Nieva
 */
public class Broadcaster {

	private static Broadcaster instance;
	public static Broadcaster Instance {
		get {
			if (instance == null)
				instance = new Broadcaster ();
			return instance;
		}
	}

	//Storage of events / observers
	private Dictionary<string, ObserverList> observerList;

	private Broadcaster(){
		this.observerList = new Dictionary<string, ObserverList> ();
	}
		
	/// <summary>
	/// Adds an event in observerList, and observerList[i]
	/// </summary>
	/// <param name="eventName">Name of the event</param>
	/// <param name="action">Method without parameter</param>
	public void AddObserver(string eventName, System.Action<Parameter> action){
		ObserverList observer;
		//if observer already exists, adds it to observerList[i]
		//So that when the event is called, all events with eventName is called
		if (this.observerList.ContainsKey (eventName)) {
			observer = this.observerList [eventName];
			observer.AddObserver (action);
		} else {
		//if observer does not exist, created an observer, and adds eventName to observer and observerlist
			observer = new ObserverList ();
			observer.AddObserver (action);

			observerList.Add (eventName, observer);
		}
	}

	/// <summary>
	/// Adds an event in observerList, and observerList[i]
	/// </summary>
	/// <param name="eventName">Name of the event</param>
	/// <param name="action">Method with parameter</param>
	public void AddObserver(string eventName, System.Action action){
		ObserverList observer;
		//if observer already exists, adds it to observerList[i]
		//So that when the event is called, all events with eventName is called
		if (this.observerList.ContainsKey (eventName)) {
			observer = this.observerList [eventName];
			observer.AddObserver (action);
		} else {
		//if observer does not exist, created an observer, and adds eventName to observer and observerlist
			observer = new ObserverList ();
			observer.AddObserver (action);

			observerList.Add (eventName, observer);
		}
	}	


	/// <summary>
	/// Removes all events with eventName
	/// </summary>
	/// <param name="eventName">Name of the Event to remove</param>
	public void RemoveObserver(string eventName){
		if (observerList.ContainsKey (eventName)) {
			ObserverList observer = observerList [eventName];
			observer.RemoveAllObservers ();
			observerList.Remove (eventName);
		}
	}

	/// <summary>
	/// Calls event of eventName with parameters
	/// </summary>
	/// <param name="eventName">Name of the Event to remove</param>
	/// <param name="parameters">Parameters of the event</param>
	public void PostEvent(string eventName, Parameter parameters){
		//if event exists
		if (this.observerList.ContainsKey (eventName)) {
			ObserverList observer = observerList [eventName];
			observer.NotifyObservers (parameters);
		}
	}

	/// <summary>
	/// Calls event of eventName without parameters
	/// </summary>
	/// <param name="eventName">Name of the Event</param>
	public void PostEvent(string eventName){
		if (this.observerList.ContainsKey (eventName)) {
			ObserverList observer = observerList [eventName];
			observer.NotifyObservers ();
		}
	}


}
