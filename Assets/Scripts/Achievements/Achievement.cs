﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * Achievement Data - stores the data of an Achievement
 * Created by Dyan Nieva
 */

[CreateAssetMenu(fileName = "PlayerType", menuName = "Game/Achievement", order = 1)]
public class Achievement : ScriptableObject {

	public string tag;
	public string achivementTitle;
	public string achievementDescription;
	public bool isUnlocked = false;

	/// <summary>
	/// Loads all Achievement data in Resources/Achievements folder
	/// </summary>
	/// <returns>All Achievements</returns>
	public static Achievement[] LoadAll() {
		return Resources.LoadAll<Achievement>("Achievements");
	}
}
