﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * RunnerType for all runner enemies
 * Created by Dyan Nieva
 */
[CreateAssetMenu(fileName = "EnemyType", menuName = "Game/Enemy/Runner", order = 2)]
public class RunnerType : EnemyType {

	/// <summary>
	/// Loads all RunnerType data in Resources/Enemies folder
	/// </summary>
	/// <returns>All RunnerType</returns>
	/// 
	public static RunnerType[] LoadAll() {
		return Resources.LoadAll<RunnerType>("Enemies");
	}

}
