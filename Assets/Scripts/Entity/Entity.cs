﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;



/*
 * Superclass for all Entities (Player / Enemy)
 * Created by Dyan Nieva
 */ 
public class Entity : MonoBehaviour {

	public const string TYPE_KEY = ""; //Key for the Entity Type
	public const string TAG = ""; //Unique tag (Player  / Enemy)

	private float maxHP = 100;
	[SerializeField] protected float currentHP = 100;
	[SerializeField] protected bool isDead;

	[SerializeField] protected EntityType type;

	[SerializeField] HPBar hp;

	// Use this for initialization
	void Start () {
		currentHP = 100;
	}
		
	// Update is called once per frame
	void Update () {
		//requests for hp bar to follow this entity
		hp.FollowEntity (this);
	}

	/// <summary>
	/// Sets the type of the entity.
	/// </summary>
	/// <param name="param">Contains EntityType accessible using <c>TYPE_KEY</c>.</param>
	protected virtual void SetEntityType(Parameter param){
		type = (EntityType)param.GetObjectExtra (TYPE_KEY);
		SetAttributes (type);

	}

	/// <summary>
	/// Sets the attribute absed from EntityType
	/// </summary>
	/// <param name="type">Type of the Entity</param>
	protected virtual void SetAttributes(EntityType type){
		
	}	

	/// <summary>
	/// Determines whether this entity is dead given the specified damage.
	/// </summary>
	/// <returns><c>true</c> if entity is dead; otherwise, <c>false</c>.</returns>
	/// <param name="damage">Damage dealth on the entity</param>
	public virtual bool IsDead(float damage){
		SetCurrentHP (damage);

		if (currentHP <= 0)
			isDead = true;

		return isDead;
	}

	/// <summary>
	/// Sets the current HP given the <c>change</c> parameter
 	/// </summary>
	/// <param name="change">Change in HP in percentage (1-100)</param>
	void SetCurrentHP(float change){
		float decreaseInHP = maxHP * (change/100);
		currentHP = currentHP - decreaseInHP;

		//updates hp bar
		hp.UpdateFill (currentHP);
	}

	/// <summary>
	/// Repositions entity back to local origin, and resets stats (hp and dead)
	/// </summary>
	public virtual void ResetEntity(){
		transform.localPosition = Vector3.zero;
		transform.rotation = Quaternion.identity;

		currentHP = maxHP;
		isDead = false;

		//updates hp bar
		hp.UpdateFill (currentHP);
	}

	public float getDamage(){
		return type.damage;
	}
}
