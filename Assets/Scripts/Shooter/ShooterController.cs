﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * ShooterController. Parent class for EnemyShooter and PlayerShooter.
 * Created By Dyan Nieva
 */
public class ShooterController : MonoBehaviour {
	
	[SerializeField] protected Entity source; //Source of the shooter. (Enemy / Player)

	protected static string TAG = "SHOOTER"; //for the object pool
	protected ObjectPool objectPooler;

	protected bool isShooting; //if the shooter can shoot
	protected float shootTimer = 0;
	protected bool canRestartTimer;
	protected bool timeIsUp;
	protected float shootDelay; //how long is the delay before shooter shoots again
	protected float bulletShootDuration; //how far does the shooter travel to meet the distance

	protected bool isInitialized = false;

	// Use this for initialization
	void Start () {
		objectPooler = ObjectPool.Instance;
		shootTimer = shootDelay;
	}

	protected virtual void Update(){

		timeIsUp = StartTimer ();

		//if the player is shooting, the timer can continue
		if (isShooting && GameManager.Instance.IsPlaying()) {	
			canRestartTimer = true;
			if (timeIsUp) { //if the timer is up, spawn bullet
				ShootBullet ();
			}
		}
	}

	/// <summary>
	/// Setting speeds for shootSpeed and bulletSpeed
	/// </summary>
	/// <param name="shootSpeed">How fast does the shooter shoot</param>
	/// <param name="bulletSpeed">How fast the bullet travel</param>
	public virtual void SetShootSpeed(float shootDelay, float bulletShootDuration){
		this.shootDelay = shootDelay;
		this.bulletShootDuration = bulletShootDuration;
		isInitialized = true;
	}


	/// <summary>
	/// Restarts the shoot timer.
	/// </summary>
	public virtual void RestartShootTimer(){
		//resetting shooting timer 
		isShooting = false; 
		shootTimer = shootDelay;
		canRestartTimer = false;
		timeIsUp = false;
	}


	/// <summary>
	/// When LMB is clicked, player shoots
	/// </summary>
	protected virtual void OnShoot(){
		isShooting = true;
	}

	/// <summary>
	/// Called by OnShoot. Spawns Bullet if shooter is shooting, and the game is playing.
	/// </summary>
	protected virtual void ShootBullet(){
		if (isShooting && GameManager.Instance.IsPlaying ()) {
			SpawnBullets ();
		}
	}


	/// <summary>
	/// Requests from the objectpool to spawn a bullet, and set its source to the respective shootercontroller
	/// </summary>
	protected virtual void SpawnBullets(){
		GameObject bullet = objectPooler.SpawnFromPool (TAG, transform.position, Quaternion.identity);
		bullet.GetComponent<Bullet> ().SetSource (this);
	}

	/// <summary>
	/// Requests from the objectpool to remove the bullet
	/// </summary>
	/// <param name="bullet">Bullet to remove</param>
	public virtual void RemoveBullet(Bullet bullet){
		objectPooler.RemoveFromPool (TAG, bullet.gameObject);
	}

	/// <summary>
	/// Times shootTimer until shootDelay. Will restart is canRestart is true.
	/// </summary>
	/// <returns><c>true</c>, if time is up, <c>false</c> otherwise.</returns>
	protected virtual bool StartTimer(){
		if (!isInitialized)
			return false;
		
		if (shootTimer <= shootDelay) {
			shootTimer += Time.deltaTime;
			return false;
		} else {
			if(canRestartTimer)
				shootTimer = 0;	
			return true;
		}
	}


	public virtual void SetSource(Entity source){
		this.source = source;
	}

	public virtual Entity GetSource(){
		return source;
	}
}
