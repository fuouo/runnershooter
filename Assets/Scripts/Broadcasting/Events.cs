﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/*
 * Storage of all event names. 
 * Created By Dyan Nieva
 */
public class Events {
	public const string ON_REPLAY = "ON_REPLAY";
	public const string ON_CHARACTER_SELECT = "ON_CHARACTER_SELECT";

	public const string ON_SHOOT = "ON_SHOOT";
	public const string ON_REST = "ON_REST";
	public const string ON_ENEMY_KILLED = "ON_ENEMY_KILLED";

	public const string ON_HIT = "ON_HIT";
	public const string ON_COLLIDE = "ON_COLLIDE";

	public const string ON_PLAY = "ON_PLAY";
	public const string ON_DEAD = "ON_DEAD";

	public const string ON_SET_PLAYER_TYPE = "ON_SET_PLAYER_TYPE";
	public const string ON_SET_ENEMY_TYPE = "ON_SET_ENEMY_TYPE";

	public const string ON_TRIGGER_ACHIEVEMENT = "ON_TRIGGER_ACHIEVEMENT";

	public const string ON_PLATFORM_SPAWN = "ON_PLATFORM_SPAWN";

	public const string ON_ADD_SCORE = "ON_ADD_SCORE";


	#region UI Audio Events
	public const string ON_INTRO = "ON_INTRO";
	public const string ON_BUTTON_CLICK = "ON_BUTTON_CLICK";
	public const string ON_WHISTLE = "ON_WHISTLE";
	public const string ON_START_RUN = "ON_START_RUN";
	public const string ON_PLAY_ACHIEVE = "ON_PLAY_ACHIEVE";
	#endregion


}