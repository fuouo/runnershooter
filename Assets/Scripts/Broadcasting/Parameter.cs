﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * Parameter class that contains all kind of parameters (primitive types, arraylist, and object)
 * Designed like Android's Intent class
 * Created By Dyan Nieva
 */
public class Parameter {

	private Dictionary <string, char> charData;
	private Dictionary <string, int> intData;
	private Dictionary <string, bool> boolData;
	private Dictionary <string, float> floatData;
	private Dictionary <string, string> stringData;
	private Dictionary<string, ArrayList> arrayListData;

	private Dictionary<string, object> objectData;

	public Parameter(){
		this.charData = new Dictionary <string, char>();
		this.intData = new Dictionary <string, int>();
		this.boolData = new Dictionary <string, bool>();
		this.floatData = new Dictionary <string, float>();
		this.stringData = new Dictionary <string, string> ();
		this.arrayListData = new Dictionary<string, ArrayList> ();

		this.objectData = new Dictionary<string, object> ();
	}


	#region PutExtra / PutObjectExtra
	//PutExtra for all primitive types, arraylist, and object data (a custom object)
	//Adds a parameters in data with the unique param name
	//param = unique parameter name
	//value = value of the parameter

	public void PutExtra(string param, char value){
		this.charData.Add (param, value);
	}

	public void PutExtra(string param, int value){
		this.intData.Add (param, value);
	}

	public void PutExtra(string param, bool value){
		this.boolData.Add (param, value);
	}
		
	public void PutExtra(string param, float value){
		this.floatData.Add (param, value);
	}

	public void PutExtra(string param, string value){
		this.stringData.Add (param, value);
	}

	public void PutExtra(string param, ArrayList value){
		this.arrayListData.Add (param, value);
	}

	public void PutExtra(string param, object value){
		this.objectData.Add (param, value);
	}

	public void PutObjectExtra(string paramName, object value) {
		this.objectData.Add(paramName, value);
	}
	#endregion 



	#region GetExtra
	//GetExtra for all primitive types, arraylist, and object data (a custom object)
	//Fetches a parameter with paramName.
	//If parameter does not exist, returns a default value instead

	public char GetCharExtra(string param, char defValue){
		if (this.charData.ContainsKey (param)) {
			return this.charData [param];
		} else {
			return defValue;
		}
	}

	public int GetIntExtra(string param, int defValue){
		if (this.intData.ContainsKey (param)) {
			return this.intData [param];
		} else {
			return defValue;
		}
	}

	public bool GetBoolExtra(string param, bool defValue){
		if (this.boolData.ContainsKey (param)) {
			return this.boolData [param];
		} else {
			return defValue;
		}
	}

	public float GetFloatExtra(string param, float defValue){
		if (this.floatData.ContainsKey (param)) {
			return this.floatData [param];
		} else {
			return defValue;
		}
	}

	public string GetStringExtra(string param, string defValue){
		if (this.stringData.ContainsKey (param)) {
			return this.stringData [param];
		} else {
			return defValue;
		}
	}

	public ArrayList GetArrayListExtra(string param, ArrayList defValue){
		if (this.arrayListData.ContainsKey (param)) {
			return this.arrayListData [param];
		} else {
			return defValue;
		}
	}
		
	public object GetObjectExtra(string paramName) {
		if(this.objectData.ContainsKey(paramName)) {
			return this.objectData[paramName];
		}
		else {
			return null;
		}
	}

	#endregion 



























}