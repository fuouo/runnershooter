﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * Superclass for all motors (PlayerMotor, and FollowMotor)
 * Created by Dyan Nieva
 */ 
public class EntityMotor : MonoBehaviour {

	//Animation Keys
	public const string IS_RUNNING = "isRunning";

	protected Vector3 moveVector; 
	protected float fwMovementDuration; //forward movement speed
	protected float lrMovementDuration; //left right / sideway mvement speed

	protected float fwDistance, lrDistance;

	void Start(){
		fwDistance = Vector3.Distance (PlatformSpawner.Instance.getPlatformSize().z * Vector3.forward, Vector3.forward);
		lrDistance = Vector3.Distance (PlatformSpawner.Instance.getPlatformSize().x * Vector3.right, Vector3.right);
	}

	//[SerializeField] protected bool isRunning = true; //Used for Animations
	// Update is called once per frame
	public virtual void Update () {
		if (!GameManager.Instance.IsPlaying ()) {
			return;
		}

		//if the game is playing, move
		Move ();
	
	}

	protected virtual void Move(){
		Debug.Log ("Move");
	}

	public void SetSpeeds(float fwMovementDuration, float lrMovementDuration){
		this.fwMovementDuration = fwMovementDuration;
		this.lrMovementDuration = lrMovementDuration;
	}


}
