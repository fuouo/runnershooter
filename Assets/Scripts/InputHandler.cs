﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * Handles keyboard and mouse input detection
 * Created by Dyan Nieva
 */

public class InputHandler : MonoBehaviour {

	bool firstTimePress = false; //To Check if this is the first time pressing input
	bool firstTimeShoot = false;

	bool canClickMouse = true;


	private static InputHandler instance;
	public static InputHandler Instance {
		get {
			return instance;
		}
	}

	void Awake(){
		instance = this;
	}

	// Update is called once per frame
	void Update()
	{
		if (Input.anyKey) {
			if (!firstTimePress) {
				//Triggers the Achievement "Detect Input";
				Parameter param = new Parameter ();
				param.PutExtra (AchievementManager.ACHIEVEMENT_TAG_KEY, "DETECT_INPUT");
				Broadcaster.Instance.PostEvent (Events.ON_TRIGGER_ACHIEVEMENT, param);
				firstTimePress = true;
			}
		}

		if (!GameManager.Instance.IsPlaying ())
			return;

		//Handle input if and only if the game is ready to play
	
		if (Input.GetMouseButtonDown (0)) { //Shoots bullet from the player if LMB is clicked
			if (canClickMouse) {
				Parameter parameter = new Parameter ();
				parameter.PutExtra (Player.TAG, Player.TAG);
				Broadcaster.Instance.PostEvent (Events.ON_SHOOT);
			}
		}

		if (Input.GetMouseButtonUp (0)) { //Stop shooting bullets from player if LMB is up
			Broadcaster.Instance.PostEvent (Events.ON_REST);
		}
			

	}

	public bool CanClickMouse(){
		return canClickMouse;
	}

	public void SetClickMouse(bool canClickMouse){
		this.canClickMouse = canClickMouse;
	}
}
