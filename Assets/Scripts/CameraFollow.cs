﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/* 
 * Manages the following of the camera with the player.
 * Created by Dyan Nieva
 */
public class CameraFollow : MonoBehaviour {

	[SerializeField] private Transform lookAt; //Object Camera will follow and look at
	private Vector3 startOffset; 

	// Use this for initialization
	void Start () {
		startOffset = lookAt.position - transform.position;
	}
	
	// Update is called once per frame
	void Update () {
		//Folows the player. Also constrains the camera to only follow the y and z axis of the player
		transform.position = new Vector3 (0, lookAt.position.y, lookAt.position.z) + -startOffset;
	}
}
