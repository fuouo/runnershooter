﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * PlayerType for players
 * Is both a runner and a shooter.
 */
[CreateAssetMenu(fileName = "PlayerType", menuName = "Game/Player", order = 1)]
public class PlayerType : EntityType {
	public float shootDelay;
	public float bulletShootDuration;

	//duration to move sideway in one platform
	public float lrMovementDuration;

	/// <summary>
	/// Loads all PlayerType data in Resources/Players folder
	/// </summary>
	/// <returns>All Players</returns>
	public static PlayerType[] LoadAll() {
		return Resources.LoadAll<PlayerType>("Players");
	}


}
