# Runner Shooter
A Runner-Shooter game, where the main character (a bunny) runs and shoots all monster that goes in its way. 

***Notes***: 
The game is originally built for mobile. However, due to lack of machine to test the APK with, the game was built as an EXE file for PC that automatically resizes in a 9:16 resolution (specifically 450 x 800).

## Controls

* **LMB** to Shoot. 
* **A** or **D** to Move Left / Right
 
## Achievement and How to Unlock them

* **Detecting Input!** Press any key (mouse/keyboard) at the start of the game (at the main menu)
* **First Time Shooting** Press LMB for the first time to shoot for the first time
* **First Enemy Killed** During the main game, kill any enemy
 

## Outside Assets:
**A big thank you to the following asset owners**: 

1. PI Entertainment LTD for the 3D Character Mesh and Animations : [Level 1 Monster Pack](https://assetstore.unity.com/packages/3d/characters/creatures/level-1-monster-pack-77703)

2. Pandomoniac for the GUI Asset Kit (GUI Art and Sounds) : [Ultimate GUI Kit](https://assetstore.unity.com/packages/tools/gui/ultimate-gui-kit-19193)

3. AxeyWorks for the Low Poly Environment Models : [Low Poly Free Pack](https://assetstore.unity.com/packages/3d/environments/low-poly-free-pack-58821)

4. Sharkshock for the free Obelix font : [Stupid Meeting / Obelix Font](https://www.dafont.com/stupid-meeting.font)