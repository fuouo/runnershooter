﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * List of all events & actions of the same event name
 * Created By Dyan Nieva
 */
public class ObserverList {

	private List<System.Action<Parameter>> eventListeners;
	private List<System.Action> eventListenersNoParams;

	public ObserverList(){
		this.eventListeners = new List<System.Action<Parameter>> ();
		this.eventListenersNoParams = new List<System.Action> ();
	}
		
	/// <summary>
	/// Adds an event with parameters.
	/// </summary>
	/// <param name="action">Action.</param>
	public void AddObserver(System.Action<Parameter> action){
		if (!isEventExisting(action))
			eventListeners.Add (action);
	}

	/// <summary>
	/// Adds an event without parameters
	/// </summary>
	/// <param name="action">Action/Method without parameter</param>
	public void AddObserver(System.Action actionNoParam){
		if (!isEventExisting(actionNoParam))
			eventListenersNoParams.Add (actionNoParam);
	}


	/// <summary>
	/// Remove one event with parameters
	/// </summary>
	/// <param name="action">Action/Method with parameters</param>
	public void RemoveObserver(System.Action<Parameter> action){
		if(isEventExisting(action))
			eventListeners.Remove(action);
	}

	/// <summary>
	/// Remove one event without parameters
	/// </summary>
	/// <param name="action">Action/Method without parameter</param>
	public void RemoveObserver(System.Action actionNoParam){
		if(isEventExisting(actionNoParam))
			eventListenersNoParams.Remove (actionNoParam);
	}

	/// <summary>
	/// Remove all recorded events
	/// </summary>
	public void RemoveAllObservers(){
		eventListeners.Clear ();
		eventListenersNoParams.Clear ();
	}

	/// <summary>
	/// Calls all events (with parameters) with the same eventName
	/// </summary>
	public void NotifyObservers(Parameter parameters) {
		for(int i = 0; i < this.eventListeners.Count; i++) {
			System.Action<Parameter> action = this.eventListeners[i];
			action(parameters);
		}
	}

	/// <summary>
	/// Calls all events (without parameters) with the same eventName
	/// </summary>
	public void NotifyObservers() {
		for(int i = 0; i < this.eventListenersNoParams.Count; i++) {
			System.Action action = this.eventListenersNoParams[i];
			action();
		}
	}

	/// <summary>
	/// Checks if event exists in eventListenersNoParam
	/// </summary>
	/// <returns><c>true</c>, if event exists, <c>false</c> otherwise.</returns>
	/// <param name="action">Action/Method without parameter</param>
	private bool isEventExisting(System.Action action){
		if(eventListenersNoParams.Contains(action)){
			return true;
		}
		return false;
	}

	/// <summary>
	/// Checks if event exists in eventListeners
	/// </summary>
	/// <returns><c>true</c>, if event exists, <c>false</c> otherwise.</returns>
	/// <param name="action">Action/Method with parameter</param>
	private bool isEventExisting(System.Action<Parameter> action){
		if (eventListeners.Contains (action)) {
			return true;
		}
		return false;
	}

	/// <summary>
	/// Gets the length of the all event listeners.
	/// </summary>
	/// <returns>The sum of events with and without parameters.</returns>
	public int GetAllEventListenersLength(){
		return (eventListeners.Count + eventListenersNoParams.Count);
	}

	/// <summary>
	/// Gets the length of the event listeners with parameters
	/// </summary>
	/// <returns>The count of all event listeners with parameters</returns>
	public int GetEventListenersLength(){
		return eventListeners.Count;
	}

	/// <summary>
	/// Gets the length of the event listeners without parameters.
	/// </summary>
	/// <returns>The count of all event listeners w/o parameters</returns>
	public int GetEventListenersNoParamsLength(){
		return eventListenersNoParams.Count;
	}

}

