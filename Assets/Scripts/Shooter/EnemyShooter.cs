﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * EnemyShooter. Shooter class for the enemy. 
 * This class automatically shoots the player.
 * The shooter continues to shoot if the player/target is within min and max distance
 * Created By Dyan Nieva
 */ 
public class EnemyShooter : ShooterController {

	[SerializeField] private GameObject target;

	float maxDistance; //relative to the source position
	float minDistance; //relative to the source position

	// Use this for initialization
	void Start () {
		TAG = "ENEMY_SHOOTER";
		objectPooler = ObjectPool.Instance;
		isShooting = false;
		shootTimer = shootDelay;

	}

	// Update is called once per frame
	protected virtual void Update () {
		if (isWithinBounds ()) { //if enemy is within valid bounds of player, shoot
			OnShoot ();
			base.Update ();

		}



	}


	/// <summary>
	/// Checks if the bullet is within the bounds of minDistance and maxDistance
	/// </summary>
	/// <returns><c>true</c>, if within bullet is within bounds, <c>false</c> otherwise.</returns>
	bool isWithinBounds(){
		Vector3 target = source.GetComponent<FollowMotor>().getTarget().position;
		float distance = Vector3.Distance (target, transform.position);
		if (distance < minDistance || distance > maxDistance) 
			return false;

		return true;

	}
		
	/// <summary>
	/// Requests from the object pool to spawn a bullet, sets its source and initialize speed.
	/// (Called from its parent class, ShooterController).
	/// </summary>
	protected override void SpawnBullets(){
		GameObject bullet = objectPooler.SpawnFromPool (TAG, transform.position, Quaternion.identity);
		bullet.GetComponent<Bullet> ().SetSource (this);
		bullet.GetComponent<Bullet> ().SetDuration (bulletShootDuration);
	}


	/// <summary>
	/// Setting min and max distance
	/// </summary>
	/// <param name="minDistance">Minimum distance.</param>
	/// <param name="maxDistance">Max distance.</param>
	public virtual void SetDistance(float minDistance, float maxDistance){
		this.minDistance = minDistance;
		this.maxDistance = maxDistance;
	}

}
