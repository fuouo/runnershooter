﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/*
 * Handles damaging collisions
 * Created by Dyan Nieva
 */

public class HitHandler : MonoBehaviour {

	//KEYS for Collider and Source of the collision, used for paramters
	public const string COLLIDER = "COLLIDER";
	public const string SOURCE = "SOURCE";

	// Use this for initialization
	void Start () {
		Broadcaster.Instance.AddObserver (Events.ON_HIT, this.OnHit); //CAlled when bullet hits an entity (From Bullet's OnTriggerEnter)
		Broadcaster.Instance.AddObserver (Events.ON_COLLIDE, this.OnCollide); //Called when enemy collides with player (From Player's OnCollisionEnter)
	}


	/// <summary>
	/// Checks if the collisin is valid (if collider is enemy), and damages source based from collider's damage
	/// </summary>
	/// <param name="param">Parameter that contains reference for both Collider and Source</param>
	void OnCollide(Parameter param){
		GameObject collider = (GameObject)param.GetObjectExtra (COLLIDER);
		GameObject source = (GameObject)param.GetObjectExtra (SOURCE);

		if (!isValidCollision (collider, source))
			return;

		Entity entityCollider = collider.GetComponent<Entity> ();
		Entity entitySource = source.GetComponent<Entity> ();

		if (collider.tag == Enemy.TAG && source.tag == Player.TAG) { //if player collides with enemy
			if (entitySource.IsDead (entityCollider.getDamage ()))
				Broadcaster.Instance.PostEvent (Events.ON_DEAD);
		}

	}

	/// <summary>
	/// Checks if the collisin is valid. Damages the collider based from source's damage
	/// <summary>
	/// <param name="param">Parameter that contains reference for both Collider and Source</param>
	void OnHit(Parameter param){
		GameObject collider = (GameObject)param.GetObjectExtra (COLLIDER);
		GameObject source = (GameObject)param.GetObjectExtra (SOURCE);

		if (!isValidCollision (collider, source))
			return;

		Entity entityCollider = collider.GetComponent<Entity> ();
		Entity entitySource = source.GetComponent<Entity> ();

		//if collider is enemy, and enemy is dead
		if (collider.tag == Enemy.TAG && entityCollider.IsDead(entitySource.getDamage())) {
			Broadcaster.Instance.PostEvent (Events.ON_ENEMY_KILLED);
			collider.gameObject.SetActive (false);
		}


		//if collider is player, and player is dead
		if (collider.tag == Player.TAG && entityCollider.IsDead(entitySource.getDamage())) {
			Broadcaster.Instance.PostEvent (Events.ON_DEAD);
		}
	}

	/// <summary>
	/// Checks if collision is valid (1) if collider is not source, (2) if collider is not floor / untagged
	/// </summary>
	/// <returns><c>true</c>, if collision was valid, <c>false</c> otherwise.</returns>
	/// <param name="collider">Collider GameObject</param>
	/// <param name="source">Source GameObject</param>
	bool isValidCollision(GameObject collider, GameObject source){
		//if collider and collidee is Player, or the floor = Don't hit / damage anything.
		if (collider.tag == source.tag || collider.tag == "Floor" || collider.tag == "Untagged") {
			return false;
		}

		return true;
	}
}
