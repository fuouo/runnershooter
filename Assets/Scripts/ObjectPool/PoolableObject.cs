﻿
using UnityEngine;


/*
 * Interface for all poolable objects
 * Created by Dyan Nieva
 */
public interface IPoolableObject {

	/// <summary>
	/// On request for an object in pool, this is called for each object to pool. 
	/// This will act like the object spawned's Start method.
	/// </summary>
	void OnObjectSpawn();

	/// <summary>
	/// On request for an object in pool, this is called for each object to pool. 
	/// This will act like the object spawned's Start method. 
	/// <param name="objectRef">Object Reference that object needs to know before spawning</param>
	/// </summary>
	void OnObjectSpawn(object objectRef);
}
