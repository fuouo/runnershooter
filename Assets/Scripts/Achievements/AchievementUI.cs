﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/*
 * Achievement UI, used by an Achievement Popup Prefab
 * Created by Dyan Nieva
 */
public class AchievementUI : MonoBehaviour, IPoolableObject {

	[SerializeField] float secondsUntilExit; //delay before achievement popup disappears
	[SerializeField] Text achievementTitle;

	public void OnObjectSpawn () {
	}

	/// <summary>
	/// Sets the title text of the Achievement Pop up based from <c>objectRef</c>
	/// </summary>
	/// <param name="objectRef">Object that contains data of triggered Achievement</param>
	public void OnObjectSpawn(object objectRef){
		Achievement a = (Achievement)objectRef;
		achievementTitle.text = a.achivementTitle;
		transform.localScale = Vector3.one;

		StartCoroutine (startExit ());
	}


	/// <summary>
	/// Delays the exit of the achievement popup
	/// </summary>
	IEnumerator startExit(){
		yield return new WaitForSeconds (secondsUntilExit);
		ObjectPool.Instance.RemoveFromPool ("ACHIEVEMENT", this.gameObject);
	}

}
