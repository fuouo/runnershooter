﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * Enemy Class, child of Entity
 * Created By Dyan Nieva
 */
public class Enemy : Entity {

	public const string TAG = "Enemy";

	void Start(){
		SetAttributes (base.type);
	}



	/// <summary>
	/// Setting attributes from EntityType
	/// </summary>
	/// <param name="type">EntityType of the Enemy (Runner/Shooter)</param>
	protected override void SetAttributes(EntityType type){
		type = (EnemyType)type;
		//Sets the speed for EntityMotor
		GetComponent<EntityMotor> ().SetSpeeds (type.fwMovementDuration, 0);
		//Sets the distance for the FollowMotor of the enemy
		GetComponent<FollowMotor> ().SetDistance (((EnemyType)type).minDistanceFromPlayer, ((EnemyType)type).maxDistanceFromPlayer);

		if (type is ShooterType) {
			//Creates a shooter as a child of Enemy gameobject
			GameObject shooter = Instantiate (((ShooterType)type).shooter,
						                     transform.position,
						                     transform.rotation,
											 transform);
			//Sets speed of shoot and bullet
			shooter.GetComponent<EnemyShooter> ().SetShootSpeed (((ShooterType)type).shootDelay, ((ShooterType)type).bulletShootDuration);
			//Sets the distance for the FollowMotor of the enemy
			shooter.GetComponent<EnemyShooter> ().SetDistance (((ShooterType)type).minDistanceFromPlayer, ((ShooterType)type).maxDistanceFromPlayer);
			//Sets source of the shooter (To whose is the shooter)
			shooter.GetComponent<EnemyShooter> ().SetSource (this);

		}

		//Instantiates the model prefab as a child of Enemy gameObject
		var obj = Instantiate (type.model, transform.position, transform.rotation);
		obj.transform.SetParent (transform);
	}

}
