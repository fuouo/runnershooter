﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * Motor class that follows the target. Used by the enemies 
 */
public class FollowMotor : EntityMotor {

	[SerializeField] Transform target;
	private float minDistanceFromPlayer; //distance from player until it stops following
	private float maxDistanceFromPlayer; //distance from player it starts following

	protected override void Move(){
		if (!ShouldFollow ())
			return; 

		//adjusts the forward vector of the object to follow target
		transform.LookAt (target);
		transform.eulerAngles = new Vector3 (0, Mathf.Clamp (transform.eulerAngles.y, 135, 225), 0);

		//moves target based from forwardSpeed
		if (fwMovementDuration == 0)
			return;
		float speed = (fwDistance / fwMovementDuration);
		transform.position = Vector3.MoveTowards (transform.position, target.position, speed * Time.deltaTime);
	}

	/// <summary>
	/// Checks whether object is not within distance from target. 
	/// </summary>
	/// <returns><c>true</c>, valid distance, <c>false</c> otherwise.</returns>
	bool ShouldFollow(){
		//if the entity is within the valid from player, start following
		float distance = Vector3.Distance (target.transform.position, transform.position);
		if (distance < minDistanceFromPlayer || distance > maxDistanceFromPlayer) 
			return false;

		return true;
		
		return true;
	}

	public Transform getTarget(){
		return target;
	}

	/// <summary>
	/// Setting min and max distance
	/// </summary>
	/// <param name="minDistanceFromPlayer">Minimum distance.</param>
	/// <param name="maxDistanceFromPlayer">Max distance.</param>
	public virtual void SetDistance(float minDistanceFromPlayer, float maxDistanceFromPlayer){
		this.minDistanceFromPlayer = minDistanceFromPlayer;
		this.maxDistanceFromPlayer = maxDistanceFromPlayer;
	} 
}
