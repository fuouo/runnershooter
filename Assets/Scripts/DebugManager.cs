﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/* This class is built solely for debugging features in the game using input 
 * If you want to enter debugMode, set this class to active
 * Created by Dyan Nieva
 */
public class DebugManager : MonoBehaviour {

	string TAG = "[DEBUG] ";


	[SerializeField] GameObject Point1Detector; //This gameobject is used to check distances between two points;
	[SerializeField] GameObject Point2Detector; //This gameobject is used to check distances between two points;


	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

		DebugInput (); //only used if GameManager's debugMode is true. Used to test features without following the game mechanics. 
		
	}

	/// <summary>
	/// Debugs features of the game using keyboard input.
	/// </summary>
	void DebugInput(){


	
		if (Input.GetKeyDown (KeyCode.Minus)) {
			Debug.Log (TAG + "Game Over");
			Broadcaster.Instance.PostEvent (Events.ON_DEAD);
		}
	
		if (Input.GetKeyDown (KeyCode.Alpha0)) {
			Debug.Log (TAG + "Replaying");
			Broadcaster.Instance.PostEvent (Events.ON_REPLAY);
		}

		if (Input.GetKeyDown (KeyCode.Alpha1)) {
			Debug.Log (TAG + "Spawn Platform");
			Broadcaster.Instance.PostEvent (Events.ON_PLATFORM_SPAWN);
		}


		if (Input.GetKeyDown (KeyCode.Alpha2)) {
			Debug.Log (TAG + "Adding Score");
			Broadcaster.Instance.PostEvent (Events.ON_ADD_SCORE);
		}

		if (Input.GetKeyDown (KeyCode.Alpha3)) {
			Debug.Log (TAG + "Calculating Distance between point 1 and 2");
			CheckDistance ();


		}

		if (Input.GetKeyDown (KeyCode.Alpha4)){
			Debug.Log (TAG + "Clamping point 1 to -45 to 45 degrees");
			ClampRotation ();


		}
	}

	/// <summary>
	/// Check distance between two objects
	/// </summary>
	void CheckDistance(){
		float distance = Vector3.Distance (Point1Detector.transform.position, Point2Detector.transform.position);
		Debug.Log (TAG + "Distance: " + distance);

		distance = Point1Detector.transform.position.z - Point2Detector.transform.position.z;
		Debug.Log (TAG + "Diff in Z: " + distance);


	}

	/// <summary>
	/// Check angle between two points
	/// </summary>
	void ClampRotation(){ 	
		Point1Detector.transform.eulerAngles = new Vector3 (0, Mathf.Clamp (Point1Detector.transform.eulerAngles.y, -45, 45), 0);

	}
}
