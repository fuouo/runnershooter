﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/*
 * Parent class for all screens.
 * Can Show and Hide itself.
 * Stores the animation for the respective screen.
 * Created by Dyan Nieva
 */ 
public class UIScreen : MonoBehaviour {

	public virtual void Show(){
		gameObject.SetActive (true);
		//TODO: insert show animation here
	}

	public virtual void Hide(){
		gameObject.SetActive (false);
		//TODO: insert hide animation here

	}

}
