﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * Bullet for all bullet objects. 
 * Has data of its speed, and its source (what shooter did it come from)
 * Dies once its beyond its maxDistance
 * Created By Dyan Nieva
 */ 
public class Bullet: MonoBehaviour, IPoolableObject {

	private ShooterController shooterSource; //source of the bullet. what shooter did the bullet spawn from

	private bool isShooting;
	[SerializeField] private float bulletShootDuration;
	[SerializeField] private float maxDistance;
	private Vector3 forward;

	/// <summary>
	/// On request, isShooting is set to true. 
	/// </summary>
	public void OnObjectSpawn () {
		//
		isShooting = true;
	}

	public void OnObjectSpawn (object obj) {
	}

	void Update () {
		
		float distance = Vector3.Distance (shooterSource.transform.position, transform.position);
		if (distance > maxDistance)
			RemoveBullet (); //bullet is removed once its beyond the maxDistance

		//if bullet is shooting / was requested by objectpooler, bullet moves
		if (isShooting) {

			float speed = (maxDistance / bulletShootDuration);
			transform.Translate (shooterSource.transform.forward * speed * Time.deltaTime);
		}
	}		


	/// <summary>
	/// If bullet collides with an entity (not Floor), notifies HitHandler, and removes bullet
	/// </summary>
	void OnTriggerEnter(Collider collision){
		if (collision.gameObject.tag == "Floor") {
			return;
		}

		if (collision.tag == shooterSource.tag) {
			return;
		}

		Parameter param = new Parameter ();
		param.PutObjectExtra (HitHandler.COLLIDER, collision.gameObject);
		param.PutObjectExtra (HitHandler.SOURCE, shooterSource.GetSource().gameObject);

		Broadcaster.Instance.PostEvent (Events.ON_HIT, param);

		RemoveBullet ();
	}

	public void SetSource(ShooterController shooterSource){
		this.shooterSource = shooterSource;
	}

	/// <summary>
	/// Sets the speed of the bullet
	/// </summary>
	/// <param name="bulletShootDuration">New duration of the bullet from EntityType</param>
	public void SetDuration(float bulletShootDuration){
		this.bulletShootDuration = bulletShootDuration;
	}

	/// <summary>
	/// Removes the bullet from the pool
	/// </summary>
	void RemoveBullet(){
		isShooting = false;
		shooterSource.RemoveBullet (this);
	}
}
