﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * Incharge of the automatic running of the player
 * Created by: Dyan Nieva
 */

public class PlayerMotor : EntityMotor {
	/// <summary>
	/// Move the Player forward (auto), and sideways (manual through keyboard input).
	/// </summary>
	protected override void Move(){
		moveVector = Vector3.zero;

		//X - Left and Right. Follow the press of the mouse. 
		moveVector.x = Input.GetAxis ("Horizontal") * (lrDistance / lrMovementDuration);

		// Z - Forward. Auto Run.
		moveVector.z = (fwDistance / fwMovementDuration);

		//moves the player based from moveVector
		transform.Translate (moveVector * Time.deltaTime);
	}



}
