﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/*
 * Player class, child of Entity. 
 */
public class Player : Entity {


	[SerializeField] Transform spawnPoint; //The origin / first position of the player
	[SerializeField] PlayerShooter shooter;

	private GameObject model;

	public const string TAG = "Player";
	public const string TYPE_KEY = "PLAYER_TYPE";

	void Start(){
		Broadcaster.Instance.AddObserver (Events.ON_SET_PLAYER_TYPE, this.SetEntityType); //called when player is selected
		Broadcaster.Instance.AddObserver (Events.ON_REPLAY, this.ResetEntity); //called when game is replayed
		Broadcaster.Instance.AddObserver (Events.ON_CHARACTER_SELECT, this.ResetObject); //called when game is replayed


	}

	/// <summary>
	/// Sets the type of the entity.
	/// </summary>
	/// <param name="param">Player type to set Player with</param>
	protected override void SetEntityType(Parameter param){
		type = (EntityType)param.GetObjectExtra (TYPE_KEY);
		SetAttributes (type);
	}

	/// <summary>
	/// Set attributes of players according to the <c>type</c>
	/// </summary>
	/// <param name="type">Player type to set Player with</param>
	protected override void SetAttributes(EntityType type){
		type = (PlayerType)type;
		//Set the speed for the player motor / running speed
		GetComponent<PlayerMotor> ().SetSpeeds (type.fwMovementDuration,  ((PlayerType)type).lrMovementDuration);

		//Sets the speed and source of the shooter
		shooter.SetShootSpeed (((PlayerType)type).shootDelay, (((PlayerType)type).bulletShootDuration));
		shooter.SetSource (this);

		//Instantiates the model of the player
		var obj = Instantiate (type.model, transform.position, transform.rotation);
		obj.transform.SetParent (transform);
		model = obj;
	}

	/// <summary>
	/// Tracks what objects player is collided with. 
	/// </summary>
	/// <remarks>If Player collides with an enemy, player is damaged. Otherwise, do nothing.
	/// </remarks>
	/// <param name="collision">Collider</param>
	void OnCollisionEnter(Collision collision){
		if (collision.gameObject.tag == "Floor") {
			return;
		}
			
		if (collision.gameObject.tag == gameObject.tag) {
			return;
		}

		Parameter param = new Parameter ();
		param.PutObjectExtra (HitHandler.COLLIDER, collision.gameObject);
		param.PutObjectExtra (HitHandler.SOURCE, gameObject);

		//informs HitHandler that a collision between player and enemy occured
		Broadcaster.Instance.PostEvent (Events.ON_COLLIDE, param);

		collision.gameObject.SetActive (false);
	}


	/// <summary>
	/// Resets the entity, and repositions Player back to spawn point position.
	/// </summary>
	public override void ResetEntity(){
		base.ResetEntity ();
		Debug.Log ("Resetting Player");
		transform.position = spawnPoint.position;
		shooter.RestartShootTimer ();

	}

	/// <summary>
	/// Resets the entire object, including the model;
	/// </summary>
	void ResetObject(){
		ResetEntity ();
		GameObject.Destroy (model);
	}
}
