﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/*
 * Platform, in charge of deciding what enemy combination to use, and the position relative to the previous platform
 * Created By Dyan Nieva
 */
public class Platform : MonoBehaviour, IPoolableObject {

	[SerializeField] Collider floorCollider; //to be used for knowing the size of the floor for spawning
	[SerializeField] EnemyCombination[] enemyCombinations; //contains all enemy combinations (bat at right, slime at back, etc)
	[SerializeField] EnemyCombination selectedEnemyCombination; 


	public void OnObjectSpawn () {
		SpawnPlatform ();
		SpawnEnemies ();
	}

	public void OnObjectSpawn (object obj) {
	}

	/// <summary>
	/// Decides what enemy combination to pick (thru RNG), and spawns enemy combination
	/// </summary>
	void SpawnEnemies(){
		//hiding all enemy combinations
		foreach (EnemyCombination enemyCombination in enemyCombinations) {
			enemyCombination.gameObject.SetActive (false);
		}

		//choosing what random index of enemycombination to use
		int rand =  Random.Range(0, enemyCombinations.Length);
		selectedEnemyCombination = enemyCombinations [rand];
		selectedEnemyCombination.gameObject.SetActive (true);

		//activates all enemies in selected enemy combination
		selectedEnemyCombination.ResetAllEnemies();
	}

	/// <summary>
	/// Spawns the next platform based from previous platform's position, and size
	/// </summary>
	void SpawnPlatform(){
		Platform previous = PlatformSpawner.Instance.getPreviousPlatform ().GetComponent<Platform>(); 
		//Gets the length of the previous' platform
		float length = ((BoxCollider)previous.getCollider ()).bounds.size.z;

		//Positions new platform at the sum of previous platform position and its length
		transform.position = previous.transform.position + Vector3.forward * length;
		transform.rotation = previous.transform.rotation;
	}

	/// <summary>
	/// Returns the Floor Collider of the platform (in any case the bounds or other attributes of the collider needs to be known)
	/// </summary>
	public Collider getCollider(){
		return floorCollider;
	}
}
